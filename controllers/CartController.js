const Cart = require("../models/Cart");
const Product = require("../models/Product");

// Create a cart
module.exports.createCart = (request_body) => {
  let new_cart_item = new Cart({
    userId: request_body.userId,
  });

  return new_cart_item
    .save()
    .then((created_cart_item) => {
      return {
        message: "Cart created successfully!",
        cartItem: created_cart_item,
      };
    })
    .catch((error) => {
      return {
        message: error.message,
      };
    });
};

// Add to cart
module.exports.addToCart = (request_body) => {
  const userId = request_body.userId;
  const productName = request_body.productName;

  return Cart.findOne({ userId: userId })
    .then((cart) => {
      if (!cart) {
        return {
          message: "Cart not found.",
        };
      }

      return Product.findOne({ name: productName }).then((product) => {
        if (!product) {
          return {
            message: "Product not found.",
          };
        }

        const existingCartItem = cart.items.find(
          (item) => item.product.toString() === product._id.toString()
        );

        if (existingCartItem) {
          existingCartItem.quantity += 1;
          existingCartItem.subtotal = product.price * existingCartItem.quantity;
        } else {
          cart.items.push({
            product: product._id,
            quantity: 1,
            subtotal: product.price,
          });
        }

        cart.total = 0;
        for (const item of cart.items) {
          const productPrice = product.price;
          item.subtotal = productPrice * item.quantity;
          cart.total += item.subtotal;
        }

        return cart.save().then((updated_cart) => {
          return {
            message: "Product added to cart.",
            cart: updated_cart,
          };
        });
      });
    })
    .catch((error) => {
      return {
        message: error.message,
      };
    });
};

// Change product quantity in cart
module.exports.changeQuantity = (request_body) => {
  const userId = request_body.userId;
  const productName = request_body.productName;
  const newQuantity = request_body.newQuantity;

  return Cart.findOne({ userId: userId })
    .then((cart) => {
      if (!cart) {
        return {
          message: "Cart not found.",
        };
      }

      return Product.findOne({ name: productName }).then((product) => {
        if (!product) {
          return {
            message: "Product not found.",
          };
        }

        const cartItem = cart.items.find(
          (item) => item.product.toString() === product._id.toString()
        );

        if (!cartItem) {
          return {
            message: "Product not found in cart.",
          };
        }

        cartItem.quantity = newQuantity;
        cartItem.subtotal = product.price * newQuantity;

        // Update total
        cart.total = cart.items.reduce(
          (total, item) => total + item.subtotal,
          0
        );

        return cart.save().then((updated_cart) => {
          return {
            message: "Product quantity updated.",
            cart: updated_cart,
          };
        });
      });
    })
    .catch((error) => {
      return {
        message: error.message,
      };
    });
};

// Remove product from cart
module.exports.removeProduct = (request_body) => {
  const userId = request_body.userId;
  const productName = request_body.productName;

  return Cart.findOne({ userId: userId })
    .then((cart) => {
      if (!cart) {
        return {
          message: "Cart not found.",
        };
      }

      return Product.findOne({ name: productName }).then((product) => {
        if (!product) {
          return {
            message: "Product not found.",
          };
        }

        cart.items = cart.items.filter(
          (item) => item.product.toString() !== product._id.toString()
        );

        // Update total
        cart.total = cart.items.reduce(
          (total, item) => total + item.subtotal,
          0
        );

        return cart.save().then((updated_cart) => {
          return {
            message: "Product removed from cart.",
            cart: updated_cart,
          };
        });
      });
    })
    .catch((error) => {
      return {
        message: error.message,
      };
    });
};

// Get all cart items
module.exports.getCartItems = (request, response) => {
  return Cart.find()
    .populate("userId")
    .then((cartItems) => {
      return cartItems;
    })
    .catch((error) => {
      return {
        message: error.message,
      };
    });
};
