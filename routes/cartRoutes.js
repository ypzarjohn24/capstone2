const express = require("express");
const router = express.Router();
const CartController = require("../controllers/CartController.js");
const auth = require("../auth.js");

// Create a cart
router.post("/", auth.verify, (request, response) => {
  CartController.createCart(request.body).then((result) => {
    response.send(result);
  });
});

// Add to cart
router.post("/add-product", auth.verify, (request, response) => {
  CartController.addToCart(request.body).then((result) => {
    response.send(result);
  });
});

// Change product quantity in cart
router.post("/change-quantity", auth.verify, (request, response) => {
  CartController.changeQuantity(request.body).then((result) => {
    response.send(result);
  });
});

// Remove product from cart
router.post("/remove-product", auth.verify, (request, response) => {
  CartController.removeProduct(request.body).then((result) => {
    response.send(result);
  });
});

// Get all cart items
router.get("/all", auth.verify, auth.verifyAdmin, (request, response) => {
  CartController.getCartItems(request.body).then((cartItems) => {
    response.send(cartItems);
  });
});

module.exports = router;
