const express = require("express");
const router = express.Router();
const OrderController = require("../controllers/OrderController.js");
const auth = require("../auth.js");

// Create an order
router.post("/", auth.verify, (request, response) => {
  OrderController.createOrder(request.body).then((result) => {
    response.send(result);
  });
});

// Get all orders
router.get("/all", auth.verify, (request, response) => {
  OrderController.getOrders(request, response);
});

// Get a specific order by ID
router.get("/:id", auth.verify, auth.verifyAdmin, (request, response) => {
  OrderController.getOrderById(request, response);
});

// Update an order by ID
router.put("/update/:id", auth.verify, (request, response) => {
  OrderController.updateOrder(request, response);
});

// Delete an order by ID
router.delete("/delete/:id", auth.verify, (request, response) => {
  OrderController.deleteOrder(request, response);
});

module.exports = router;
