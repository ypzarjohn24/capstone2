const mongoose = require("mongoose");

const cart_item_schema = new mongoose.Schema({
  product: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Product",
    required: true,
  },
  quantity: { type: Number, default: 1 },
  subtotal: {
    type: Number,
    default: 0,
  },
});

const cart_schema = new mongoose.Schema({
  userId: { type: String, required: true },
  items: [cart_item_schema],
  total: {
    type: Number,
    default: 0,
  },
});

module.exports = mongoose.model("Cart", cart_schema);
